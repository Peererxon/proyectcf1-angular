import { Component, OnInit } from '@angular/core';
import { CarroDeComprasService } from './../servicios/carro-de-compras.service';



@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css']
})
export class PagoComponent implements OnInit {
  nombrecompleto:string = "";
  correo:string         = "";
  nombreUsuario:string  = "";
  clave:string          = "";
  cedula:string         = "";
  constructor(private almacen:CarroDeComprasService) { }

  ngOnInit() {
  }

  public pagar(){
   alert("compra exitosa");
   this.almacen.almacen.splice(0,this.almacen.almacen.length);
  }

}
