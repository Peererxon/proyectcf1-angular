import { Component, OnInit} from '@angular/core';
import {CuentasService} from '../servicios/cuentas.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  nombrecompleto:string = "";
  correo:string         = "";
  nombreUsuario:string  = "";
  clave:string          = "";
  cedula:string         = "";
  constructor(private _cuentaService:CuentasService,) {
    }

  ngOnInit() {
  }

  public validarExistencia(){
    if ( this.validarCampos() ) {
      let existente=this._cuentaService.buscarPorCorreo( this.correo );
      if ( existente >-1 ) {
        alert("Este correo ya se encuentra registrado");
        }else if( this._cuentaService.buscarPorUsuario(this.nombreUsuario) >-1 ){
          alert("Este nombre de usuario se encuentra en uso");
        }else{
          alert("Registro exitoso");
          this._cuentaService.agregarCuenta
            (
              this.nombrecompleto.toLocaleLowerCase(),
              this.correo,
              this.nombreUsuario,
              this.clave,
            );
            this.nombreUsuario = this.correo = this.nombrecompleto = this.clave = this.cedula = "";

        }
    }
  }

  public validarCampos(){
    if (this.nombrecompleto==""|| this.nombrecompleto.length<6) {
      alert("Debe ingresar su nombre completo (nombre + apellido) ")
      return false
    }else if (this.correo=="") {
      alert("Debe ingresar un correo electronico")
        return false
      }else if (this.nombreUsuario=="") {
          alert("Debe ingresar un nombre de usuario")
          return false
        }else if (this.clave=="") {
            alert("Debe ingresar una clave")
            return false
          }else
                return true
  }
}

/**
 *                                        REVISAR
 * Comprobar si el navegador es compatible

  Podemos ver si el navegador web tiene disponible la funcionalidad del localstorage así:
  if (typeof(Storage) !== "undefined") {
      // LocalStorage disponible
  } else {
      // LocalStorage no soportado en este navegador
  }
  Guardar datos en el navegador

  Para almacenar datos y guardar nuevos elementos o indices en el LocalStorage usaremos la siguiente instrucción:
  // Guardar
  localStorage.setItem("titulo", "Curso de Angular avanzado - Víctor Robles");

  De esta manera damos de alta un nuevo elemento en el almacenamiento del browser.
  Recuperar datos

  Para conseguir los datos que tenemos guardados en un indice de nuestro almacenamiento local del navegador usaremos:
  // Conseguir elemento
  localStorage.getItem("titulo");

  Esto nos devolverá el valor que hemos guardado anteriormente.
  Guardar objetos en el LocalStorage

  Para guardar un objeto primero debemos convertirlo en un string json ya que el localstorage no permite guardar objetos de JavaSciprt como tal.

  Tendríamos que hacer algo así:
  localStorage.setItem("usuario", JSON.stringify(mi_objeto));

  Guardamos el elemento usuario cuyo valor es un objeto convertido a string.
  Sacar objetos del LocalStorage

  Para recuperar un objeto primero debemos convertirlo en un objeto de JavaScript json en lugar del string json que hay guardado por defecto.

  Haríamos esto:
  JSON.parse(localStorage.getItem("usuario"));

  Recordemos que:
  JSON.parse() es para parsear o convertir algo a un objeto JSON usable por JavaScript.
  JSON.stringify() es para crear un JSON string de un objeto o un array.
  Borrar o vaciar el localStorage

  Para eliminar un elemento del localStorage haremos:
  localStorage.removeItem("titulo");

  Para eliminar todas las variables guardadas en el localStorage haremos:
  localStorage.clear();

  Con esto ya hemos visto lo fundamental para trabajar con el almacenamiento local (localStorage) en nuestras aplicaciones web 😉
 */
