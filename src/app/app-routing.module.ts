import { NosotrosComponent } from './nosotros/nosotros.component';
import { IniciarComponent } from './iniciar/iniciar.component';
import { DetalleComponent } from './detalle/detalle.component';
import { HomeComponent } from './home/home.component';
import { RegistroComponent } from './registro/registro.component';
//import { NotFoundComponent } from './not-found/not-found.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactoComponent } from './contacto/contacto.component';
import { CarritoComprasComponent } from './carrito-compras/carrito-compras.component';
import { PagoComponent } from './pago/pago.component';


const routes: Routes = [
  {path:"",redirectTo:'home',pathMatch:'full'},
  {path:"detalle/:parametro",component:DetalleComponent},
  {path:"home",component:HomeComponent},
  {path:"registro",component:RegistroComponent},
  {path: "iniciar",component:IniciarComponent},
  {path: "contacto",component:ContactoComponent},
  {path:"nosotros",component:NosotrosComponent},
  {path: "carro", component:CarritoComprasComponent},
  {path: "pago", component:PagoComponent}

  //{path:"**",component:NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
