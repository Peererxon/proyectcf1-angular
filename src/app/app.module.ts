import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule }   from '@angular/forms';


import { AppComponent } from './app.component';
import { RegistroComponent } from './registro/registro.component';
//import { ProductosComponent } from './productos/productos.component';
import { HomeComponent } from './home/home.component';
import { DetalleComponent } from './detalle/detalle.component';
import { IniciarComponent } from './iniciar/iniciar.component';
import { ContactoComponent } from './contacto/contacto.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { SlickModule } from 'ngx-slick';
import { HttpClientModule } from '@angular/common/http';
//import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CarritoComprasComponent } from './carrito-compras/carrito-compras.component';
import { PagoComponent } from './pago/pago.component';


@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    //ProductosComponent,
    HomeComponent,
    DetalleComponent,
    IniciarComponent,
    ContactoComponent,
    NosotrosComponent,
    CarritoComprasComponent,
    PagoComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule, 
    //ToastModule.forRoot(),
    AppRoutingModule,
    SlickModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
