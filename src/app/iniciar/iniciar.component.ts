import { Component, OnInit } from '@angular/core';
import { CuentasService } from '../servicios/cuentas.service';

@Component({
  selector: 'app-iniciar',
  templateUrl: './iniciar.component.html',
  styleUrls: ['./iniciar.component.css']
})
export class IniciarComponent implements OnInit {
  nombreUsuario:string = "";
  clave:string = "";
  constructor(public _cuentas : CuentasService) { }

  ngOnInit() {
    console.log(this._cuentas.ingreso);
  }

  ValidarEntrada(){
    if(this._cuentas.ValidarEntrada(this.nombreUsuario,this.clave)){
      this._cuentas.ingreso=true;
      alert("Inicio de sesion completado")
      this.nombreUsuario = this.clave = "";
    }else{
      alert("usuario o clave invalida");
    }

  }

}
