import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../servicios/productos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public productos         :any[] = [];
  public CategoriasApi     :any[] = [];
  public ProductosApi      :any[] = [];
  public ProductosApiFilter:any[] = [];

  constructor(private productoService:ProductosService) {
   }
  
  ngOnInit() {
    console.log("home funciona!");
    let observable = this.productoService.getCategoriasAPI();

    observable.subscribe( (result:any)=>{
      this.CategoriasApi = result
    } )
    console.log("estas son las categorias que retorna el API:"+this.CategoriasApi);
    /* this.productos = this.productoService.getProductos();
    console.table(this.productos); */

    let observable2 = this.productoService.getProductosApi();
    this.productoService.agregarProductoApi(observable2);
    observable2.subscribe( (result:any)=>{
      this.ProductosApi = result;
      this.productos= this.ProductosApi;
      this.ProductosApiFilter= this.ProductosApi;
    } )


  }
  slideConfig = {"slidesToShow": 4, "slidesToScroll": 4,"arrows":true};
  
  
  MostrarProductosByCat(id){
    console.log("id enviado:"+id);
    if (id!=="15") {
      console.log("estas dentro del condicional id<>15")
      console.table("arreglo antes de ser filtrado: "+ this.ProductosApi);
      this.productos =  this.ProductosApiFilter.filter(function(product){
        return product.categories[0].id == parseInt(id);
      
      });
      console.log("before of:" +this.productos);

    }else{
      this.productos=this.ProductosApi;
    }
    
  }
}


/*$('.slick4').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});*/

