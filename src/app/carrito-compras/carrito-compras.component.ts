import { CarroDeComprasService } from './../servicios/carro-de-compras.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carrito-compras',
  templateUrl: './carrito-compras.component.html',
  styleUrls: ['./carrito-compras.component.css']
})
export class CarritoComprasComponent implements OnInit {
  public _almacen:  any[];
  constructor
  (
    private ShopCar: CarroDeComprasService
  ) { 
    this._almacen=this.ShopCar.almacen;
    }

  ngOnInit() {
  }

  public eliminarDelCarro(id:string){
    this.ShopCar.eliminarDelCarrito(id);
    this.ShopCar.almacen;
  }
}
