import { CarroDeComprasService } from './../servicios/carro-de-compras.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductosService } from '../servicios/productos.service';
import { CuentasService } from '../servicios/cuentas.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {
  public productoActual:any;
  public identificador :any;
  public validado      :any;
  constructor
  (
    private parametro:ActivatedRoute,
    private productoService:ProductosService,
    private ShopCar:CarroDeComprasService,
    private _cuentas: CuentasService,
  ) { }

  ngOnInit() {
    this.identificador=this.parametro.snapshot.paramMap.get('parametro');
    console.log(this.identificador);
    let observable =  this.productoService.getProductoById(this.identificador);
    this.validado = this._cuentas.ingreso;

    observable.subscribe( (result:any)=>{
      this.productoActual = result;
      console.log(this.productoActual);
    } )

  }

  public addToShopCar(name:string,id:string,$:string,img:string){
      this.ShopCar.agregarAlCarrito(name,id,parseInt($),img);
      console.log(this.ShopCar.almacen);
      if(this.ShopCar.almacen.length>0){
        alert("Producto agregado correctamente");
      }
  }


  }

