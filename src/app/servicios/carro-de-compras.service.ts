import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarroDeComprasService {
  almacen :Shopcar []=[];
  constructor() {
    console.log("haz llamado a cuentas provider, y estos son los productos agregados hasta el momento",
     + this.almacen);
   }

   public agregarAlCarrito(name:string,id:string,price:number,img:string){
    this.almacen.push({
      nombre        : name,
      id            : id,
      //cantidadTotal : ca,
      precio        : price,
      img           : img
    });
   }
   
   public eliminarDelCarrito(id:string){
     let posicion :number = this.almacen.findIndex(producto => producto.id ==id);
     if(posicion>-1){
      this.almacen.splice(posicion,1);
     }else{
       alert("algo hiciste mal!, revisa el provider animal del monte");
     }
   }
}

class Shopcar{
  nombre        :string;
  id            :string;
  //cantidadTotal :number;
  precio        :number;
  img           :string;

  constructor(name:string,id:string,ca:number,price:number,img:string){
    this.nombre        = name;
    this.id            = id;
    //this.cantidadTotal = ca;
    this.precio        = price;
    this.img           = img;

  }
}
