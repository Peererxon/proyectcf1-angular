import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ProductosService {
/*   productos :Producto[]=[{
    nombre      : "Ps4 pro",
    precio      : 900,
    imagen      :"../../assets/img/ps4 pro.jpg",
    cantidad    : 11,
    estado      :"Nuevo",
    descripcion :"lorem lorem lorem",
    descuento   :false,
    id:10
  },
  {
    nombre      : "Ps5 new",
    precio      : 800,
    imagen      :"../../assets/img/ps4 pro.jpg",
    cantidad    : 7,
    estado      :"Nuevo",
    descripcion :"lorem lorem lorem",
    descuento   :true,
    id:11
  },
  {
    nombre      : "Collar",
    precio      : 40,
    imagen      :"../../assets/img/corazones.jpg",
    cantidad    : 30,
    estado      :"Nuevo",
    descripcion :"lorem lorem lorem",
    descuento   :false,
    id:12
  },
  {
    nombre      : "Ps5 new",
    precio      : 800,
    imagen      :"../../assets/img/ps4 pro.jpg",
    cantidad    : 7,
    estado      :"Nuevo",
    descripcion :"lorem lorem lorem",
    descuento   :true,
    id:13
  },
  {
    nombre      : "Ps5 new",
    precio      : 800,
    imagen      :"../../assets/img/ps4 pro.jpg",
    cantidad    : 7,
    estado      :"Nuevo",
    descripcion :"lorem lorem lorem",
    descuento   :true,
    id:14
  },
  {
    nombre      : "Collar",
    precio      : 30,
    imagen      :"../../assets/img/guitarra.png",
    cantidad    : 20,
    estado      :"Usado",
    descripcion :"lorem lorem lorem",
    descuento   :true,
    id:15
  }]; */

  productosApi:any[]=[];
  productoApi :any     ;

  constructor(private http:HttpClient) {

  }

/*   public getProductos(){
    return this.productos;
  } */

  public getCategoriasAPI(){
    let url="https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products/categories?";
    let ck="consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
    let cs="consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";

    console.log(this.http.get(url+ck+"&"+cs));

    return this.http.get(url+ck+"&"+cs);
    //return this.http.get("https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products/categories/19?consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3&consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff")
  }

  public getProductosApi(){
    let url="https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products?";
    let ck="consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
    let cs="consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";
    
    return this.http.get(url+ck+"&"+cs);
  }

  public agregarProductoApi(productoActual:any){
    this.productoApi=productoActual;
  }

   getProductoById(id){
    let url="https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products/";
    let ck="consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
    let cs="consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";
    return this.http.get(url+id+"?"+ck+"&"+cs);
    }

}


/*   public getProductosByPrice(precio){
    for(let i=0;i<this.productos.length;i++){
      if (this.productos[i].precio===Number.parseInt(precio))
        return this.productos[i];
    }
  } */

/*   public getProductosById(id){
    for(let i=0;i<this.productosApi.length;i++){
      if (this.productosApi[i].id==Number.parseInt(id))
        return this.productosApi[i];
    }

    
  } */
  /**
   *                            REVISAR
   * https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products?consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3&consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff
   * Esto retorna todos los productos de la API
   */

/* class Producto{
  nombre:string;
  precio:number;
  imagen:string;
  cantidad:number;
  estado:string;
  descripcion:string;
  descuento:boolean;
  id:number;

    constructor(
      nombre:string,
      precio:number,
      imagen:string,
      cantidad:number,
      estado:string,
      descripcion:string,
      descuento:boolean
      )
      {
        this.nombre=nombre;
        this.precio=precio;
        this.imagen=imagen;
        this.cantidad=cantidad;
        this.estado=estado;
        this.descripcion=descripcion;
        this.descuento=descuento;

      }

        if (this.descuento==true) {
          this.precio=this.precio*0.75;
      }
}

 */
