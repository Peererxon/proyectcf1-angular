import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CuentasService {
  ingreso:boolean = false;
  cuentas:Usuario[]=[{
    nombreCompleto : "Anderson Gil",
    correo:"anderson111903@ionic.com",
    nombreUsuario:"peererxon",
    clave  :"14112000",
  }];
  constructor() {
    console.log('Hello CuentasProvider Provider');
  }

 public agregarCuenta(nc:string,co:string,nu:string,cla:string){
   this.cuentas.push({
     nombreCompleto : nc,
     correo:co,
     nombreUsuario:nu,
     clave  :cla,
   });
  }

  public buscarPorCorreo(correo){
    let resultado=this.cuentas.findIndex(usuario=> usuario.correo===correo);
      return resultado;
  }

  public buscarPorUsuario(userName){
    let resultado=this.cuentas.findIndex(usuario=> usuario.nombreUsuario===userName);
      return resultado;
  }

  public ValidarEntrada(userName,clave){
    let resultado=this.cuentas.findIndex(usuario=> usuario.nombreUsuario===userName);
    if (resultado>-1){
      if (this.cuentas[resultado].clave===clave) {
        return true
        }else{
        return false
        }
    }else{
      return false
    }
  }

 }


class Usuario{
  nombreCompleto:string;
  correo:string;
  nombreUsuario:string;
  clave:string;
    constructor(nc,co,nu,cla,f){
      this.nombreCompleto=nc;
      this.correo=co;
      this.nombreUsuario=nu;
      this.clave=cla;
    }
}
